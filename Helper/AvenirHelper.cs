﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Misc.AvenirTelecom.Helper
{
    public class AvenirHelper
    {
        public static string MakeEngToRussAttributeInfo(string engAttributeInfo)
        {
            var russAttribInfo = engAttributeInfo.Replace("Subscription plan", "план подписки");

            russAttribInfo = russAttribInfo.Replace("Subscription type", "Тип подписки");
            russAttribInfo = russAttribInfo.Replace("1 year subscription", "1 год подписки");
            russAttribInfo = russAttribInfo.Replace("2 years subscription", "2 года подписки");
            russAttribInfo = russAttribInfo.Replace("Without subscription", "Без подписки");

            russAttribInfo = russAttribInfo.Replace("Plan type", "Тип плана");
            russAttribInfo = russAttribInfo.Replace("New service", "Новая услуга");
            russAttribInfo = russAttribInfo.Replace("Change of plan", "Смена плана");
            russAttribInfo = russAttribInfo.Replace("Prima subscription", "подписка Prima");
            russAttribInfo = russAttribInfo.Replace("Without Traffic Plan", "Без плана движения");

            russAttribInfo = russAttribInfo.Replace("Price type", "Тип Цена");
            russAttribInfo = russAttribInfo.Replace("Cash", "Денежные средства");
            russAttribInfo = russAttribInfo.Replace("Leash", "привязь");

            return russAttribInfo;
        }
    }
}
