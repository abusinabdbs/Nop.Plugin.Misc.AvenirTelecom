﻿var PTSimple = (function () {
    var _tabMenus = ['presentationLi', 'priceLi', 'technicalLi', 'helpLi', 'conditionsLi', 'accessoriesLi'];
    var _tabs = ['tab-presentation', 'tab-Price', 'tab-technical', 'tab-help', 'tab-conditions', 'tab-accessories'];
    var _choiceRadios = ['NewService', 'Changeofplan', 'Primasubscriptionto'];
    var _isAnyChoiceSelected = function () {
        var result = false;
        $('.choice-group input[type="radio"]').each(function (index, radio) {
            if (radio.checked) {
                result = true;
            }
        });

        return result;
    };
    var renderTableForCash = function (data) {
        $('#subsPlan').html('');
        var th = '<tr class="leasing_plans_two_years plans_tab"><th class="show_on_cash_without_contract">' + appConstants.plugin.avenir.magicConstants.subscriptionPlan + '</th>' +
            '<th>' + appConstants.plugin.avenir.magicConstants.cashPrice + '</th><th>'+appConstants.plugin.avenir.magicConstants.monthlyFeeForSubscription+'</th><th>'+appConstants.plugin.avenir.magicConstants.totalFeesPerMonth+'</th></tr>';
        $('#subsPlan').append(th);
        data.forEach(function (monthlyPlan, index) {
            monthlyPlan.Plans.forEach(function (obj, index) {
                var td1 = '<td>' +
                        '<div class="td" style="text-align: center;">' +
                            '<div class="plan-item">' +
                                '<a target="_blank" rel="CashTopicDetail_' + index + '" href="topic/topicdetails?topicId=' + obj.SubscriptionPlanTopicId + '"  class="info topicToolTip"><font><font class="">' + obj.SubscriptionPlan + '</font></font></a>' +
                            '</div>' +
                            '<div style="display:none;" id="CashTopicDetail_' + index + '">' + obj.TopicDetails + '</div>' +
                        '</div>' +
                    '</td>';
                var td2 = '<td class="cash" style="text-align: center;"><div class="td"><font><font>' + obj.CashPriceForDevice + ' </font></font></div></td>';
                var td3 = '<td><div class="td" style="text-align: center;"><div class="td"><font><font class="monthlyFee">' + obj.MonthlyFeeForSubscription + ' </font></font></div></div></td>';
                var td4 = '<td class="cash" style="text-align: center;"><div class="td"><div class="inline-group"><label for="td4" class="radio"><input type="radio" class="attributeTrack" onchange="PTSimple.startContinue(' + obj.pvavId + ',' + 100 + ');" name="' + obj.pvavId + '"><var><font><font>'
                    + obj.MonthlyFeeForSubscription + ' </font></font></var></label></div></div></td>';
                var tr = '<tr>' + td1 + td2 + td3 + td4 + '</tr>';
                $('#subsPlan').append(tr);
            });

        });
        $(".continue,.nopAjaxCartProductVariantAddToCartButton").css("display", "none");
    };
    var renderTableForLease = function (data) {
        $('#subsPlan').html('');
        var th = '<tr class="leasing_plans_two_years plans_tab"><th class="show_on_cash_without_contract">' + appConstants.plugin.avenir.magicConstants.subscriptionPlan + '</th>' +
            '<th>' + appConstants.plugin.avenir.magicConstants.cashPrice + '</th><th>' + appConstants.plugin.avenir.magicConstants.monthlyFeeForSubscription + '</th><th>' + appConstants.plugin.avenir.magicConstants.totalFeesPerMonth + '</th></tr>';
        $('#subsPlan').append(th);

        data.forEach(function (monthlyPlan, index) {
            monthlyPlan.Plans.forEach(function (obj, index) {
                var td1 = '<td>' +
                    '<div class="td" style="text-align: center;">' +
                        '<div class="plan-item">' +
                            '<a target="_blank" rel="LeaseTopicDetail_' + index + '" href="topic/topicdetails?topicId=' + obj.SubscriptionPlanTopicId + '"  class="info topicToolTip"><font><font class="">' + obj.SubscriptionPlan + '</font></font></a>' +
                        '</div>' +
                        '<div style="display:none;" id="LeaseTopicDetail_' + index + '">' + obj.TopicDetails + '</div>' +
                    '</div>' +
                    '</td>';
                var td2 = '<td class="cash" style="text-align: center;"><div class="td"><font><font>' + obj.PricePaymentDevice + ' </font></font></div></td>';
                var td3 = '<td><div class="td" style="text-align: center;"><div class="td"><font><font class="monthlyFee">' + obj.MonthlyFeeForSubscription + ' </font></font></div></div></td>';
                var td4 = '<td class="cash"><div class="td" style="text-align: center;"><div class="inline-group"><label for="td4" class="radio"><input type="radio" class="attributeTrack" onchange="PTSimple.startContinue(' + obj.pvavId + ',' + 100 + ');" name="' + obj.pvavId + '"><var><font><font>'
                    + obj.TotalFeesPerMonthForDeviceAndServicePlan + ' </font></font></var></label></div></div></td>';
                var tr = '<tr>' + td1 + td2 + td3 + td4 + '</tr>';
                $('#subsPlan').append(tr);
            });

        });
        $(".continue,nopAjaxCartProductVariantAddToCartButton").css("display", "none");
    };

    return {
        showTab: function (i) {
            _tabMenus.forEach(function (menu, index) {
                if (i === index) {
                    $('#' + _tabMenus[i]).addClass('current');
                } else {
                    $('#' + _tabMenus[index]).removeClass('current');
                }
            });

            _tabs.forEach(function (tab, index) {
                if (i === index) {
                    $('#' + _tabs[i]).css("display", "inline-block");
                } else {
                    $('#' + _tabs[index]).css("display", "none");
                }
            });
        },
        getTabs: function () {
            return _tabs;
        },
        getTabMenus: function () {
            return _tabMenus;
        },
        getChoices: function () {
            return _choiceRadios;
        },
        manageChoice: function (id, i) {
            //console.log(id);
            $('.choice-group input[type="radio"]').each(function (index, radio) {
                if (radio.id == id) {
                    radio.checked = true;
                } else {
                    radio.checked = false;
                }
            });

            $('.Plan-type input[type="radio"], .С-абонамент-за input[type="radio"]').each(function (index, obj) {
                if (index == i) {
                    obj.checked = true;
                } else {
                    obj.checked = false;
                }
            });
        },
        manageSubscriptionType: function (selectedOption) {
            if (selectedOption == 2) {
                selectedOption = 1;
            } else if (selectedOption == 1) {
                selectedOption = 2;
            } else if (selectedOption == 0) {
                selectedOption = 3;
            }

            $('.Subscription-type option, .Абонаментен-план option').each(function (index, obj) {
                if ($(obj).attr('name') == selectedOption) {
                    $('.Абонаментен-план').val($(obj).attr('value'));
                }
            });
        },
        selectChoice: function (i) {
            _choiceRadios.forEach(function (choice, index) {
                if (i == index) {
                    $('#' + _choiceRadios[index]).prop('checked', true);
                } else {
                    $('#' + _choiceRadios[index]).prop('checked', false);
                }
            });

            $('.Plan-type input[type="radio"], .С-абонамент-за input[type="radio"]').each(function (index, obj) {
                if (index == i) {
                    obj.checked = true;
                } else {
                    obj.checked = false;
                }
            });

            $('#choicePlanModal').modal('hide');
        },
        startContinue: function (pvavId, monthlyFee) {
            //localStorage.setItem("monthlyFee", monthlyFee);
            $('#subsPlan input[type="radio"]').each(function (index, radio) {
                if (radio.name != pvavId) {
                    radio.checked = false;
                }
            });
            //console.log(pvavId);
            if (_isAnyChoiceSelected()) {

            } else {
                $('#choicePlanModal').modal();
            }

            $('.Subscription-plan input[type="radio"], .План input[type="radio"]').each(function (index, radio) {
                if (radio.value == pvavId) {
                    //var test =$('input[type="radio"][value="' + pvavId + '"]').p;
                    radio.checked = true;
                } else {
                    radio.checked = false;
                }
            });

            $.ajax({
                cache: false,
                url: '/ShoppingCart/ProductDetails_AttributeChange?productId=' + productId + "&productVariantId=" + pvavId,
                data: $('#product-details-form').serialize(),
                type: 'post',
                success: function (data) {
                    if (data.price) {
                        $(".product-price").children().prop('itemprop', 'price').text(data.price);
                    }
                    $(".continue,.nopAjaxCartProductVariantAddToCartButton").css("display", "block");
                }
            });

        },

        startContinueWithoutTraffic: function (pvavId) {
            $('#subsPlan input[type="radio"]').each(function (index, radio) {
                if (radio.name != pvavId) {
                    radio.checked = false;
                }
            });
            ////console.log(pvavId);
            //if (_isAnyChoiceSelected()) {

            //} else {
            //    $('#choicePlanModal').modal();
            //}

            $('.Subscription-plan input[type="radio"], .План input[type="radio"]').each(function (index, radio) {
                if (radio.value == pvavId) {
                    radio.checked = true;
                } else {
                    radio.checked = false;
                }
            });

            $('input:radio[class=withoutTrafficPlan]').prop('checked', true);

            $.ajax({
                cache: false,
                url: '/ShoppingCart/ProductDetails_AttributeChange?productId=' + productId + "&productVariantId=" + pvavId,
                data: $('#product-details-form').serialize(),
                type: 'post',
                success: function (data) {
                    if (data.price) {
                        $(".product-price").children().prop('itemprop', 'price').text(data.price);
                    }
                    $(".continue,.nopAjaxCartProductVariantAddToCartButton").css("display", "block");
                }
            });

        },

        renderSubscriptionPlan: function (s, p) {

            var model = {
                ProductId: productId,
                SubscriptionType: s,
                PriceType: p
            };

            $.post('AvenirTelecomMonthPlan/GetMonthlyPlanTable', model, function (data) {
                //console.log(data);
                if (p == 1) {
                    renderTableForCash(data);
                    $('#continue').css('display', 'none');
                    $('a.topicToolTip').speechbubble();
                }
                if (p == 2) {
                    renderTableForLease(data);
                    $('#continue').css('display', 'none');
                    $('a.topicToolTip').speechbubble();
                }
            });

        },
        renderRegularPrice: function () {
            $('#subsPlan').html('');
            var th = '<tr class="leasing_plans_two_years plans_tab">' +
                     '<th>' + appConstants.plugin.avenir.magicConstants.subscriptionPlan + '</th>' +
                     '<th>' + appConstants.plugin.avenir.magicConstants.cashPrice + '</th>' +
                     '</tr>';
            $('#subsPlan').append(th);
            $.post('AvenirTelecomMonthPlan/GetRegularPrice', { productId: productId }, function (data) {
                var subscriptionPlan = (data[0].SubscriptionPlan.toLowerCase() == 'without tariff plan') ? appConstants.plugin.avenir.magicConstants.withoutTariffPlan : data[0].SubscriptionPlan;
                var tr = '<tr style="text-align: center;">' +
                        '<td>' + subscriptionPlan + '</td>' +
                        '<td style="text-align: center;">' +
                            '<input type="radio" class="attributeTrack" onchange="PTSimple.startContinueWithoutTraffic(' + data[0].pvavId + ');" name="' + data[0].pvavId + '">' + data[0].MonthlyFeeForSubscription +
                        '</td>' +
                    '</tr>';
                $('#subsPlan').append(tr);
            });

        }

    }
})();

$(document).ready(function () {
    $(".ui-state-active").after('<li id="ui-state-default ui-corner-top priceLi" role="tab" aria-controls="quickTab-price" tabindex="0" aria-labelledby="ui-id-4"><a class="ui-tabs-anchor priceAnch" role="presentation" tabindex="-1">Price</a></li>');
    var activeTabId = $("div").find("[aria-expanded='" + true + "']").attr('id');

    $("#" + "" + activeTabId + "").after(
        '<div id="quickTab-price" aria-labelledby="ui-id-4" class="ui-tabs-panel ui-widget-content ui-corner-bottom" role="tabpanel" style="display:none;">' +
            '<div class="plan-top">' +
                '<div class="inline-group choice-group">' +
                    '<strong>' + appConstants.plugin.avenir.magicConstants.pleaseChoose + ': </strong>' +
                    '<input type="radio" name="NewService" value="0" id="NewService" checked="checked">' + appConstants.plugin.avenir.choose.newService + ' <strong> ' + appConstants.plugin.avenir.magicConstants.or + ' </strong>' +
                    '<input type="radio" name="Changeofplan" value="1" id="Changeofplan">' + appConstants.plugin.avenir.choose.changeofplan + '<strong> ' + appConstants.plugin.avenir.magicConstants.or + ' </strong>' +
                    '<input type="radio" name="Primasubscriptionto" value="2" id="Primasubscriptionto">' + appConstants.plugin.avenir.choose.primasubscriptionto + ' &nbsp; <strong>' + appConstants.plugin.avenir.magicConstants.aSubscription + ':</strong>' +
                    '<select name="contract" id="contract">' +
                        '<option value="1">' + appConstants.plugin.avenir.subscription.oneyear + '</option>' +
                        '<option value="2" selected="selected">' + appConstants.plugin.avenir.subscription.twoyear + '</option>' +
                        '<option value="0">' + appConstants.plugin.avenir.subscription.without + '</option>' +
                    '</select>' +
                    '&nbsp;<strong>' + appConstants.plugin.avenir.magicConstants.andPrice + ': </strong>' +
                    '<select name="price" id="price">' +
                        '<option value="1" selected="selected">' + appConstants.plugin.avenir.pricetype.cash + '</option>' +
                        '<option value="2">' + appConstants.plugin.avenir.pricetype.leash + '</option>' +
                    '</select></div><div class="multiple-plans">' +
                    '<table class="responsive-table" id="subsPlan">' +
                    '</table>' +
                '</div>' +
            '</div>' +
        '</div>');

    $(".qty-input").css("display", "none");

    /*$('.Price-type, .Цена').css('display', 'none');
    $('.Plan-type, .С-абонамент-за').css('display', 'none');
    $('.Subscription-type, .Абонаментен-план').css('display', 'none');
    $('.Subscription-plan, .план-подписки').css('display', 'none');*/


    $("#btnChoose").click(function () {
        $(".ui-tabs-panel").css("display", "none");
        $("#quickTab-price").css("display", "block");
        $(".priceAnch").css("background", "#00b9f0");
        $(".ui-state-default").removeClass("ui-state-active");
    });

    $(".priceAnch").click(function () {
        $(".ui-state-default").removeClass("ui-state-active");
        $(".ui-tabs-panel").css("display", "none");
        $("#quickTab-price").css("display", "block");
        $(".priceAnch").css("background", "#00b9f0");
    });

    $(".ui-state-default").click(function () {
        $("#quickTab-price").css("display", "none");
        $(".priceAnch").css("background", "#dcdcdc");
        $(this).addClass("ui-state-active");
        var selectedTab = $(this).attr("aria-controls");
        $("#" + "" + selectedTab + "").css("display", "block");

    });

    //$(".priceLi").click(function () {
    //    $("#quickTab-price").css("display", "block");
    //});


    PTSimple.getTabMenus().forEach(function (menu, index) {
        $('#' + menu).children('a').click(function (e) {
            return PTSimple.showTab(index);
        });
    });

    PTSimple.getChoices().forEach(function (choice, index) {
        $('#' + choice).click(function () {
            return PTSimple.manageChoice(choice, index);
        });
    });

    // Init
    // 1 = 1 year subscription
    // 1 = Cash
    PTSimple.renderSubscriptionPlan(2, 1);

    $('#contract').on('change', function () {

        /*var selectedOption = $('#contract').find(":selected").val();
        var selectedPriceType = 0;
        if ($('#price').find(":selected").text() == "Cash") {
            selectedPriceType = 1
        }
        else {
            selectedPriceType = 2
        }

        PTSimple.manageSubscriptionType(selectedOption);

        if (selectedOption == 0) {
            PTSimple.renderRegularPrice();
            $("#price").prop("disabled", true);
        } else {
            $("#price").prop("disabled", false);
            PTSimple.renderSubscriptionPlan(selectedOption, selectedPriceType);
        }*/

        var selectedOption = $('#contract').val();
        var selectedPriceType = 0;
        selectedPriceType = $('#price').val();

        PTSimple.manageSubscriptionType(selectedOption);

        if (selectedOption == 0) {
            PTSimple.renderRegularPrice();
            $("#price").prop("disabled", true);
        } else {
            $("#price").prop("disabled", false);
            PTSimple.renderSubscriptionPlan(selectedOption, selectedPriceType);
        }

    });

    $('#price').on('change', function () {

        var selectedOption = 0;
        selectedOption = $('#price').val();

        $('.Price-type option, .Цена option').each(function (index, obj) {
            if ($(obj).attr('name') == selectedOption) {
                $('.Цена').val($(obj).attr('value'));
            }
        });

        PTSimple.renderSubscriptionPlan($('#contract').val(), selectedOption);

        /*var selectedOption = 0;
        if ($('#price').find(":selected").text() == "Cash") {
            selectedOption = 1;
        }
        else {
            selectedOption = 2;
        }

        $('.Price-type option').each(function (index, obj) {
            if ($(obj).attr('name') == selectedOption) {
                $('.Price-type').val($(obj).attr('value'));
            }
        });

        PTSimple.renderSubscriptionPlan($('#contract').find(":selected").val(), selectedOption);*/

    });



});

