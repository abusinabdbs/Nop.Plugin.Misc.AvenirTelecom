﻿
using System.Collections.Generic;
namespace Nop.Plugin.Misc.AvenirTelecom.Domain
{
   public class DataModel
    {
        public int ProductId { get; set; }
        public int SubscriptionType { get; set; }
        public int PriceType { get; set; }
    }

   public class MonthlyPlanViewModel
   {
       public int ProductId { get; set; }
       public string SubscriptionPlan { get; set; }
       public List<Plan> Plans { get; set; }
   }
}
