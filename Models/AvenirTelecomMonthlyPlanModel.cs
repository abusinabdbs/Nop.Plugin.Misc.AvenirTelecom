﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;
using System.Collections.Generic;

namespace Nop.Plugin.Misc.AvenirTelecom.Models
{
    public partial class AvenirTelecomMonthlyPlanModel : BaseNopEntityModel
    {

        public AvenirTelecomMonthlyPlanModel()
        {
            AvailableSubscriptionType = new List<SelectListItem>();
            AvailablePriceType = new List<SelectListItem>();
        }
        public string ProductName { get; set; }

        [NopResourceDisplayName("Admin.Misc.AvenirTelecom.Fields.ProductId")]
        [AllowHtml]
        public int ProductId { get; set; }
        [NopResourceDisplayName("Admin.Misc.AvenirTelecom.Fields.Add")]
        public int Add { get; set; }

        [NopResourceDisplayName("Admin.Misc.AvenirTelecom.Fields.Delete")]
        public int Delete { get; set; }

        [NopResourceDisplayName("Admin.Misc.AvenirTelecom.Fields.SubscriptionPlan")]
        public string SubscriptionPlan { get; set; }

        public int TopicId { get; set; }

        [NopResourceDisplayName("Admin.Misc.AvenirTelecom.CashPriceForDevice")]
        [AllowHtml]
        public decimal CashPriceForDevice { get; set; }

        [NopResourceDisplayName("Admin.Misc.AvenirTelecom.MonthlyFeeForSubscription")]
        [AllowHtml]
        public decimal MonthlyFeeForSubscription { get; set; }

        [NopResourceDisplayName("Admin.Misc.AvenirTelecom.TotalFeesPerMonthForSubscription")]
        [AllowHtml]
        public string TotalFeesPerMonthForSubscription { get; set; }

 
        public string SubscriptionType { get; set; }

        public int SubscriptionTypeId { get; set; }
        public string PriceTypeName { get; set; }

        public int PriceTypeId { get; set; }

        [NopResourceDisplayName("Admin.Misc.AvenirTelecom.PricePaymentDevice")]
        [AllowHtml]
        public string PricePaymentDevice { get; set; }

        [NopResourceDisplayName("Admin.Misc.AvenirTelecom.TotalFeesPerMonthForDeviceAndServicePlan")]
        [AllowHtml]
        public string TotalFeesPerMonthForDeviceAndServicePlan { get; set; }

        [NopResourceDisplayName("Admin.Misc.AvenirTelecom.ProductAttributeId")]
        [AllowHtml]
        public int ProductAttributeId { get; set; }

        public int ProductAttributeValueId { get; set; }


        [NopResourceDisplayName("Admin.Misc.AvenirTelecom.SubscriptionType")]
        [AllowHtml]
        public IList<SelectListItem> AvailableSubscriptionType { get; set; }

        [NopResourceDisplayName("Admin.Misc.AvenirTelecom.PriceType")]
        [AllowHtml]
        public IList<SelectListItem> AvailablePriceType { get; set; }

        public string CashPriceForDeviceOneYear { get; set; }

        public string MonthlyFeeForSubscriptionOneYear { get; set; }
        public string PricePaymentDeviceOneYear { get; set; }

        public bool SetAsDefaultPlan { get; set; }

}
}